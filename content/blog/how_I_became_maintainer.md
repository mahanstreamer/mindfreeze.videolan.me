---
author: Vibhoothi
date: '2017-05-28'
slug: how-i-build-my-first-custom-rom-and-how-i-became-maintainer
title: How I build my first Custom ROM and how I became a Maintainer
project: 'VertexOS'
---

Hello guys ,

I am back since I was lazy to type and my field of interest of mine got diverted, I was thinking that my life is not planned and going in a different way, for a few months I started thinking what should I really do, at first I thought to build custom ROM will be very hard and I cannot do that by myself. So I am a Custom Rom Maintainer and starting to be a developer. This post was in DRAFT for nearly 2 months.

![image500c](https://vibhoothiiaanand.files.wordpress.com/2017/05/https-s3-postimg-org-65w6itsgz-31_banner.png)

So, friends, I will say how I started and ended up being a guy who goes to college after that a quick fresh up, then sit in front of Laptop for more than 5hours then takes a break after that again sit for another hour or 2 which eventually makes me sleep for less than 6 hours.
So last 5 months there were so many changes, I bought a new laptop MacBook Pro 2015, started going to college and made new friends, destroyed many routines and made new. You can check my GitHub profile for my progress: [Github.](https://github.com/vibhoothi)

Today is the first day of Semester break (S2 is Finished) which is only for a month thanks to Amrita, and now I am no longer a junior, I don't know about the results of this semester, hope I will clear all.

So its about Android, Custom ROMs, first I have started with [Lineage ROM](https://lineageos.org/) [Formally known as CyanagenMod] building for OnePlus 2, then to the most painful [AOSP](https://source.android.com/) [Android Open Source Project], I own OnePlus 2, as most of the source code of device is open source, still for a starter like me it's nearly perfect thing to go ahead and  spend  some quality time on it. We will know how the things are done and where are the places which can make changes.

For building roms we need to download the source of the Rom which will be really huge and time consuming so for that we could use Google Cloud Platform for faster and easier building. The speed of the internet in GCP is really mind-blowing, minimum you will be getting around 35MB/s and almost aroun~d  76MB/s which makes me to download the source within Half an hour(Source is around 40GB-60GB).After that, we will be doing some device specific things like
 -  Kernel: which is heart of the OS, which controls how whole things should  work.
 -  Vendor the device specific files which is required for the hardware to work properly.
 - Device tree is the place where the device specific codes which links all the required files properly.
 -  Common files from the hardware manufacturer for OP2 its appeal so oppo/common, and many other Misc. Files which depends from ROM to ROM.

After building and fixing errors, we would be uploading it to the public or as private for testing purpose.

Currently, I am maintaining [VertexOS](https://github.com/VertexOS) for Oneplus 2, it's having a good response from the public, during initial Alpha releases there were less than 50 daily users when it jumped to beta and further jumped to stable releases it crossed to more than 500 users. There are three types of releases happening around one is official which will be fully stable, the second one is Test releases where there will be continuous testing of features and yet a step behind the stable release. Experimental releases which are those builds which may even not boot since all the new features will be first deployed 
There is a [XDA Thread](https://forum.xda-developers.com/oneplus-2/development/rom7-1-1-vertex-os-oneplus2-t3573408)

My XDA [Profile.](https://forum.xda-developers.com/member.php?u=6633459).

I would like to give my sincere thanks to people from telegram and slack  who helped me build the ROM and taught me. 

There are many things to say I will say in next post peeps,

Thanks for reading,

Vibhoothi


_Orignally published on [Wordpress](https://vibhoothiiaanand.wordpress.com/2017/05/29/how-i-build-my-first-custom-rom-and-how-i-became-maintainer/) on May 29, 2017_
