---
title: About
author: Vibhoothi
---

[Vibhoothi](https://www.linkedin.com/in/vibhoothi/), is holding Bachelors degree in Computer Science and Engineering from [Amrita University](https://amrita.edu).

Currently he is part of Electronic and Electrical Engineering Department of [Trinity College Dublin](https://tcd.ie) working on various Video Compression research and development focusing on improving compression and efficency of Open-standards like AV1, VP9 etc. He also part of [sigmedia.tv](http://sigmedia.tv) Group in the university under Prof. Anil Kokaram.

He is also part of various FOSS Multimedia projects including [Xiph.org](https://xiph.org) and [VideoLAN.](https://videolan.org) projects.

Email: mindfreeze@videolan.org,  mindfreeze@xiph.org,  vibhoothi.a@gmail.com
